﻿using System;
using Automatonymous;
using MassTransit;
using Warehouse.Contracts;

namespace Warehouse.Components.StateMachines
{
  public class AllocationStateMachine: MassTransitStateMachine<AllocationState>
  {
    public AllocationStateMachine()
    {
      Event(() => AllocationCreated,
        x => x.CorrelateById(m => m.Message.AllocationId));
      Event(() => AllocationReleaseRequested,
        x => x.CorrelateById(m => m.Message.AllocationId));

      Schedule(() => HoldExpiration, x => x.HoldDurationToken, s =>
      {
        s.Delay = TimeSpan.FromHours(1);

        s.Received = x => x.CorrelateById(m => m.Message.AllocationId);
      });
      
      InstanceState(x => x.CurrentState);
      
      Initially(
        When(AllocationCreated)
          .Schedule(HoldExpiration, context => context.Init<IAllocationHoldDurationExpired>(new {context.Data.AllocationId}),
            context => context.Data.HoldDuration)
          .TransitionTo(Allocated),
        When(AllocationReleaseRequested)
          .TransitionTo(Released)
      );

      During(Released,
        When(AllocationCreated)
          .ThenAsync(
            context => Console.Out.WriteLineAsync($"Allocation was already released: {context.Instance.CorrelationId}"))
          .Finalize()
      );

      During(Allocated,
        When(AllocationCreated)
          .Schedule(HoldExpiration, context => context.Init<IAllocationHoldDurationExpired>(new {context.Data.AllocationId}),
            context => context.Data.HoldDuration),
        When(HoldExpiration.Received)
          .ThenAsync(
            context => Console.Out.WriteLineAsync($"Allocation expired: {context.Instance.CorrelationId}"))
          .Finalize(),
        When(AllocationReleaseRequested)
          .Unschedule(HoldExpiration)
          .ThenAsync(context =>
            Console.Out.WriteLineAsync($"Allocation release request, granted: {context.Instance.CorrelationId}"))
          .Finalize()
      );

      SetCompletedWhenFinalized();
    }
    
    public Schedule<AllocationState, IAllocationHoldDurationExpired> HoldExpiration { get; set; }
    
    public State Allocated { get; private set; }
    public State Released { get; private set; }
    
    public Event<IAllocationCreated> AllocationCreated { get; set; }
    public Event<IAllocationReleaseRequested> AllocationReleaseRequested { get; set; }
  }
}