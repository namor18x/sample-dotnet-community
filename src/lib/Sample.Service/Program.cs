﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using MassTransit.MongoDbIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sample.Components.Consumers;
using Sample.Components.CourierActivities;
using Sample.Components.StateMachines;
using Sample.Components.StateMachines.OrderStateMachineActivities;
using Warehouse.Contracts;

namespace Sample.Service
{
  class Program
  {
    static async Task Main(string[] args)
    {
      var isService = !(Debugger.IsAttached || args.Contains("--console"));

      var builder = new HostBuilder()
        .ConfigureAppConfiguration((hostingContext, config) =>
        {
          config.AddJsonFile("appsettings.json", true);
          config.AddEnvironmentVariables();

          if (args != null)
            config.AddCommandLine(args);
        })
        .ConfigureServices((hostContext, services) =>
        {
          services.AddScoped<AcceptOrderActivity>();
          
          services.TryAddSingleton(KebabCaseEndpointNameFormatter.Instance);
          services.AddMassTransit(cfg =>
          {
            cfg.AddConsumersFromNamespaceContaining<SubmitOrderConsumer>();
            cfg.AddActivitiesFromNamespaceContaining<AllocateInventoryActivity>();
            
            cfg.AddSagaStateMachine<OrderStateMachine, OrderState>(typeof(OrderStateMachineDefinition))
              .MongoDbRepository(m =>
              {
                m.Connection = "mongodb://127.0.0.1";
                m.DatabaseName = "orders";
              });

            cfg.AddBus(ConfigureBus);
            cfg.AddRequestClient<IAllocateInventory>();
          });

          services.AddHostedService<MassTransitConsoleHostedService>();
        })
        .ConfigureLogging((hostingContext, logging) =>
        {
          logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
          logging.AddConsole();
        });

      if (isService)
        await builder.UseWindowsService().Build().RunAsync();
      else
        await builder.RunConsoleAsync();
    }

    static IBusControl ConfigureBus(IServiceProvider provider)
    {
      return Bus.Factory.CreateUsingRabbitMq(cfg =>
      {
        cfg.ConfigureEndpoints(provider);
      });
    }
  }
}