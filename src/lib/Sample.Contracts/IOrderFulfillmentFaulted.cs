﻿using System;

namespace Sample.Contracts
{
  public interface IOrderFulfillmentFaulted
  {
    Guid OrderId { get; }
    DateTime Timestamp { get; }
  }
}