﻿using System;

namespace Sample.Contracts
{
  public interface IFulfillOrder
  {
    Guid OrderId { get; }
  }
}