﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier;
using MassTransit.Courier.Contracts;
using Sample.Contracts;

namespace Sample.Components.Consumers
{
  public class FulfillOrderConsumer: IConsumer<IFulfillOrder>
  {
    public async Task Consume(ConsumeContext<IFulfillOrder> context)
    {
      var builder = new RoutingSlipBuilder(NewId.NextGuid());
      
      builder.AddActivity("AllocateInventory", new Uri("queue:allocate-inventory_execute"), new
      {
        ItemNumber = "ITEM123",
        Quantity = 10
      });
      
      builder.AddActivity("PaymentActivity", new Uri("queue:payment_execute"), new
      {
        CardNumber = "5999-1234-5678-9012",
        Amount = 99.95m
      });
      
      builder.AddVariable("OrderId", context.Message.OrderId);

      // await builder.AddSubscription(context.SourceAddress, RoutingSlipEvents.Faulted, RoutingSlipEventContents.None,
      //   x => x.Send<IOrderFulfillmentFaulted>(new
      //   {
      //     context.Message.OrderId
      //   }));

      var routingSlip = builder.Build();
      
      await context.Execute(routingSlip);
    }
  }
}