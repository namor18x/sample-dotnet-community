﻿using System;
using Automatonymous;
using MassTransit;
using Sample.Components.StateMachines.OrderStateMachineActivities;
using Sample.Contracts;

namespace Sample.Components.StateMachines
{
  public class OrderStateMachine: MassTransitStateMachine<OrderState>
  {
    public OrderStateMachine()
    {
      Event(() => OrderSubmitted, x => x.CorrelateById(m => m.Message.OrderId));
      Event(() => OrderAccepted, x => x.CorrelateById(m => m.Message.OrderId));
      Event(() => OrderFulfillmentFaulted, x => x.CorrelateById(m => m.Message.OrderId));
      Event(() => OrderStatusRequested, x =>
      {
        x.CorrelateById(m => m.Message.OrderId);
        x.OnMissingInstance(m => m.ExecuteAsync(async context =>
        {
          if (context.RequestId.HasValue)
          {
            await context.RespondAsync<IOrderNotFound>(new {context.Message.OrderId});
          }
        }));
      });
      Event(() => AccountClosed,
        x => x.CorrelateBy((instance, context) => instance.CustomerNumber == context.Message.CustomerNumber));
      
      InstanceState(x => x.CurrentState);
      
      Initially(
        When(OrderSubmitted)
          .Then(context =>
          {
            context.Instance.SubmitDate = context.Data.Timestamp;
            context.Instance.CustomerNumber = context.Data.CustomerNumber;
            context.Instance.Updated = DateTime.UtcNow;
          })
          .TransitionTo(Submitted)
        );
      
      During(Submitted, 
        Ignore(OrderSubmitted),
          When(AccountClosed)
            .TransitionTo(Canceled),
          When(OrderAccepted)
            .Activity(x => x.OfType<AcceptOrderActivity>())
            .TransitionTo(Accepted)
        );
      
      During(Accepted,
        When(OrderFulfillmentFaulted)
          .TransitionTo(Faulted));
      
      DuringAny(
        When(OrderStatusRequested)
          .RespondAsync(x => x.Init<IOrderStatus>(new
            {
                OrderId = x.Instance.CorrelationId,
                State = x.Instance.CurrentState
            }))
        );
      
      DuringAny(
        When(OrderSubmitted)
          .Then(context =>
          {
            context.Instance.SubmitDate ??= context.Data.Timestamp;
            context.Instance.CustomerNumber ??= context.Data.CustomerNumber;
          }));
    }
    
    public State Submitted { get; private set; }
    public State Accepted { get; private set; }
    public State Canceled { get; private set; }
    public State Faulted { get; private set; }
    
    public Event<IOrderSubmitted> OrderSubmitted { get; private set; }
    public Event<IOrderAccepted> OrderAccepted { get; private set; }
    public Event<IOrderFulfillmentFaulted> OrderFulfillmentFaulted { get; private set; }
    public Event<ICheckOrder> OrderStatusRequested { get; private set; }
    public Event<ICustomerAccountClosed> AccountClosed { get; private set; }
  }
}