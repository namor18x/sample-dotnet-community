﻿using System;

namespace Sample.Components.CourierActivities
{
  public interface IAllocateInventoryArguments
  {
    Guid OrderId { get; }
    string ItemNumber { get; }
    decimal Quantity { get; }
  }
}