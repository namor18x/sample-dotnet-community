﻿using System;

namespace Sample.Components.CourierActivities
{
  public interface IAllocateInventoryLog
  {
    Guid AllocationId { get; }
  }
}