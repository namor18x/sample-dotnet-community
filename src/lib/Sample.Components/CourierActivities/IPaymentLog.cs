﻿namespace Sample.Components.CourierActivities
{
  public interface IPaymentLog
  {
    string AuthorizationCode { get; }
  }
}