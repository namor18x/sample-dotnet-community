﻿using System;

namespace Sample.Components.CourierActivities
{
  public interface IPaymentArguments
  {
    Guid OrderId { get; }
    decimal Amount { get; }
    string CardNumber { get; }
  }
}