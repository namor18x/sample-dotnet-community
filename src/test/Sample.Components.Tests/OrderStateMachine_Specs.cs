﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Automatonymous.Graphing;
using Automatonymous.Visualizer;
using MassTransit;
using MassTransit.Testing;
using NUnit.Framework;
using Sample.Components.StateMachines;
using Sample.Contracts;

namespace Sample.Components.Tests
{
  [TestFixture]
  public class Submitting_an_order
  {
    [Test]
    public async Task Should_create_a_state_instance()
    {
      var harness = new InMemoryTestHarness();
      var saga = harness.StateMachineSaga<OrderState, OrderStateMachine>(new OrderStateMachine());

      await harness.Start();
      try
      {
        var orderId = NewId.NextGuid();

        await harness.Bus.Publish<IOrderSubmitted>(new
        {
          OrderId = orderId,
          InVar.Timestamp, 
          CustomerNumber = "12345"
        });

        Assert.That(saga.Created.Select(x => x.CorrelationId == orderId).Any(), Is.True);

        var sagaInstance = await saga.Exists(orderId, x => x.Submitted);
        
        Assert.That(sagaInstance, Is.Not.Null);

        var instance = saga.Sagas.Contains(sagaInstance.Value);
        Assert.That(instance.CustomerNumber, Is.EqualTo("12345"));
      }
      finally
      {
        await harness.Stop();
      }
    }
    
    [Test]
    public async Task Should_respond_to_status_checks()
    {
      var orderStateMachine = new OrderStateMachine();
      
      var harness = new InMemoryTestHarness();
      var saga = harness.StateMachineSaga<OrderState, OrderStateMachine>(orderStateMachine);

      await harness.Start();
      try
      {
        var orderId = NewId.NextGuid();

        await harness.Bus.Publish<IOrderSubmitted>(new
        {
          OrderId = orderId,
          InVar.Timestamp, 
          CustomerNumber = "12345"
        });

        Assert.That(saga.Created.Select(x => x.CorrelationId == orderId).Any(), Is.True);

        var sagaInstance = await saga.Exists(orderId, x => x.Submitted);
        Assert.That(sagaInstance, Is.Not.Null);

        var requestClient = await harness.ConnectRequestClient<ICheckOrder>();

        var response = await requestClient.GetResponse<IOrderStatus>(new {OrderId = orderId});
        Assert.That(response.Message.State, Is.EqualTo(orderStateMachine.Submitted.Name));
      }
      finally
      {
        await harness.Stop();
      }
    }
    
    [Test]
    public async Task Should_cancel_when_customer_account_closed()
    {
      var orderStateMachine = new OrderStateMachine();
      
      var harness = new InMemoryTestHarness();
      var saga = harness.StateMachineSaga<OrderState, OrderStateMachine>(orderStateMachine);

      await harness.Start();
      try
      {
        var orderId = NewId.NextGuid();

        await harness.Bus.Publish<IOrderSubmitted>(new
        {
          OrderId = orderId,
          InVar.Timestamp, 
          CustomerNumber = "12345"
        });

        Assert.That(saga.Created.Select(x => x.CorrelationId == orderId).Any(), Is.True);

        var sagaInstance = await saga.Exists(orderId, x => x.Submitted);
        Assert.That(sagaInstance, Is.Not.Null);

        await harness.Bus.Publish<ICustomerAccountClosed>(new
        {
          CustomerId = InVar.Id,
          CustomerNumber = "12345"
        });

        sagaInstance = await saga.Exists(orderId, x => x.Canceled);
        Assert.That(sagaInstance, Is.Not.Null);
      }
      finally
      {
        await harness.Stop();
      }
    }
    
    [Test]
    public async Task Should_accept_when_order_is_accepted()
    {
      var orderStateMachine = new OrderStateMachine();
      
      var harness = new InMemoryTestHarness();
      var saga = harness.StateMachineSaga<OrderState, OrderStateMachine>(orderStateMachine);

      await harness.Start();
      try
      {
        var orderId = NewId.NextGuid();

        await harness.Bus.Publish<IOrderSubmitted>(new
        {
          OrderId = orderId,
          InVar.Timestamp, 
          CustomerNumber = "12345"
        });

        Assert.That(saga.Created.Select(x => x.CorrelationId == orderId).Any(), Is.True);

        var sagaInstance = await saga.Exists(orderId, x => x.Submitted);
        Assert.That(sagaInstance, Is.Not.Null);

        await harness.Bus.Publish<IOrderAccepted>(new
        {
          OrderId = orderId,
          Timestamp = InVar.Timestamp
        });

        sagaInstance = await saga.Exists(orderId, x => x.Accepted);
        Assert.That(sagaInstance, Is.Not.Null);
      }
      finally
      {
        await harness.Stop();
      }
    }

    [Test]
    public void Show_me_the_state_machine()
    {
      var orderStateMachine = new OrderStateMachine();

      var graph = orderStateMachine.GetGraph();
      var generator = new StateMachineGraphvizGenerator(graph);
      var dots = generator.CreateDotFile();
      
      Console.WriteLine(dots);
    }
  }
}