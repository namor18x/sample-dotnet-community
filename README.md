# Saga and Routing Slip with MassTransit sample
---------------------

This sample was coded following a live-coding series by Chris Patterson available on Twitch and YouTube. For more details, see this [page](https://masstransit-project.com/getting-started/live-coding.html).

